var express = require('express');
var router = express.Router();
var db = require('../database');

router.get("/all", function(req, res) {
    db.Cat.findAll()
        .then( cats => {
            res.status(200).send(JSON.stringify(cats));
        })
        .catch( err => {
            res.status(500).send(JSON.stringify(err));
        });
});

router.get("/:id", function(req, res) {
    db.Cat.findByPk(req.params.id)
        .then( cat => {
            res.status(200).send(JSON.stringify(cat));
        })
        .catch( err => {
            res.status(500).send(JSON.stringify(err));
        });
});

router.get("/create", function(req, res) {
    db.Cat.sync()
    .then(() => db.Cat.create({
      firstName: 'Bobbie',
      lastName: 'Draper'
    }))
    .then( cat => {
        res.status(200).send(JSON.stringify(cat));
    })
    .catch( err => {
        res.status(500).send(JSON.stringify(err));
    });
});

router.put("/", function(req, res) {
    db.Cat.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        id: req.body.id
        })
        .then( cat => {
            res.status(200).send(JSON.stringify(cat));
        })
        .catch( err => {
            res.status(500).send(JSON.stringify(err));
        });
});

router.delete("/:id", function(req, res) {
    db.Cat.destroy({
        where: {
            id: req.params.id
        }
        })
        .then( () => {
            res.status(200).send();
        })
        .catch( err => {
            res.status(500).send(JSON.stringify(err));
        });
});

module.exports = router;
var express = require('express');
var router = express.Router();
const db = require('../database');

/* GET home page. */
router.get('/create', function (req, res, next) {
  db.sequelize.sync()
    .then(() => db.Cat.create({
      firstName: 'Bobbie',
      lastName: 'Draper'
    }))
    .then(theCat => {
      res.status(200).send(theCat.toJSON());
    });
});

router.get('/', function (req, res, next) {
  res.render('index', { title: 'Playground' });
});

module.exports = router;

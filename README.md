# Playground

This is a sample Node project to test the Gitlab ci pipeline. It is an Express Web App with a Postgresql database. It exposes an HTTP service on port 5000 (default for Gitlab CI) and includes a few tests with the Jest framework.

There is a [Dockerfile](./Dockerfile) to build the project and also a [docker-compose.yml](./docker-compose.yml) that includes a Postgres image with the project image.

The project is built and deployed with [Auto DevOps](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)

Auto DevOps can be customized as explained [here](https://docs.gitlab.com/ee/topics/autodevops/customize.html) with detailed instructions on GitLab CI [here](https://docs.gitlab.com/ee/ci/yaml/README.html).

const db = require('./database');

beforeAll(async () => {
    await db.sequelize.sync();
});

test('create cat', async () => {
    expect.assertions(1);
    const cat = await db.Cat.create({
        id: 1,
        firstName: 'Bobbie',
        lastName: 'Draper'
    });
    expect(cat.id).toEqual(1);
});

test('get cat', async () => {
    expect.assertions(2);
    const cat = await db.Cat.findByPk(1);
    expect(cat.firstName).toEqual('Bobbie');
    expect(cat.lastName).toEqual('Draper');
});

test('delete cat', async () => {
    expect.assertions(1);
    await db.Cat.destroy({
        where: {
            id: 1
        }
    });
    const cat = await db.Cat.findByPk(1);
    expect(cat).toBeNull();
});

afterAll(async () => {
    await db.sequelize.close();
});
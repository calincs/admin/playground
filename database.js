const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL);
const Cat = sequelize.define('Cat', {
    firstName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastName: {
        type: Sequelize.STRING,
        allowNull: true
    },
});
module.exports = {
    sequelize: sequelize,
    Cat: Cat
};